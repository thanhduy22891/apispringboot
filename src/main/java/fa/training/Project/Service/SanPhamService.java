package fa.training.Project.Service;

import java.util.List;

import fa.training.Project.Entity.SanPham;

public interface SanPhamService {

	List<SanPham> getAllProducts();
}
