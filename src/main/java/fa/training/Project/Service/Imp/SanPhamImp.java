package fa.training.Project.Service.Imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fa.training.Project.Entity.SanPham;
import fa.training.Project.Repository.SanPhamRepository;
import fa.training.Project.Service.SanPhamService;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class SanPhamImp implements SanPhamService {

	@Autowired
	private SanPhamRepository sanPhamRepository;

	@Override
	public List<SanPham> getAllProducts() {
		return sanPhamRepository.findAll();
	}

}
