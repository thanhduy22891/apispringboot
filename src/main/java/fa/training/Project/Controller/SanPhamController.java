package fa.training.Project.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fa.training.Project.Entity.SanPham;
import fa.training.Project.Service.Imp.SanPhamImp;

@RestController
@CrossOrigin
@RequestMapping("/api/home")
public class SanPhamController {

	@Autowired
	SanPhamImp sanPham;

	@GetMapping("/product")
	public List<SanPham> getAllProducts() {
		return (List<SanPham>) sanPham.getAllProducts();
	}

}
