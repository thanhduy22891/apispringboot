package fa.training.Project.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import fa.training.Project.Entity.Users;
import fa.training.Project.Repository.UsersRepository;
import fa.training.Project.form.Login;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class UsersController {

	@Autowired
	private UsersRepository userRepository;

	@PostMapping("/user")
	Users newUser(@RequestBody Users newUser) {
		System.err.println(newUser);
		return userRepository.save(newUser);
	}

	@GetMapping("/users")
	List<Users> getAllUsers() {
		return userRepository.findAll();
	}

	@PostMapping("/login")
	public ResponseEntity<Users> login(@RequestBody Login loginData) {
		String email = loginData.getEmail();
		String password = loginData.getPassword();

		Users user = userRepository.findByEmailAndPassword(email, password);

		return ResponseEntity.ok().body(user);
	}

	// Các phần khác của UsersController

}
