package fa.training.Project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fa.training.Project.Entity.Danh_Muc_SP;

	@Repository
	public interface Danh_Muc_SP_Repository extends JpaRepository<Danh_Muc_SP, Integer>{

	
}
