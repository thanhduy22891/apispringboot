package fa.training.Project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fa.training.Project.Entity.TinhTrangThanhToan;

	
	@Repository
	public interface TinhTrangThanhToanRepository extends JpaRepository<TinhTrangThanhToan, Integer>{

	
}
