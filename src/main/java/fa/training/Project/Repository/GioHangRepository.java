package fa.training.Project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fa.training.Project.Entity.GioHang;


	@Repository
	public interface GioHangRepository extends JpaRepository<GioHang, Integer>{

	
}
