package fa.training.Project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fa.training.Project.Entity.Users;

	
@Repository
public interface UsersRepository extends JpaRepository<Users, Integer>{

	Users findByEmailAndPassword(String email, String password);
}
