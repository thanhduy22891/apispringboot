package fa.training.Project.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fa.training.Project.Entity.ThanhToan;


	@Repository
	public interface ThanhToanRepository extends JpaRepository<ThanhToan, Integer>{

	
}
