package fa.training.Project.Repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import fa.training.Project.Entity.SanPham;

@Repository
public interface SanPhamRepository extends JpaRepository<SanPham, Integer>{

	public List<SanPham> findAll();
}
