package fa.training.Project.Entity;


import java.util.List;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

	@Entity
	@Table(name = "tinh_trang_thanh_toan")
	@AllArgsConstructor
	@NoArgsConstructor
	@Getter
	@Setter
	public class TinhTrangThanhToan {

	    @Id
	    @Column(name = "tttt")
	    private int tttt;

	    @Column(name = "ten_tinh_trang", nullable = false)
	    private String tenTinhTrang;

	    @OneToMany(mappedBy = "tinhTrang", cascade = CascadeType.ALL)
	    private List<ThanhToan> thanhToans;
	

}
