package fa.training.Project.Entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "DanhMucSanPham")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Danh_Muc_SP {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idDanhMuc")
    private int idDanhMuc;

    @Column(name = "name_DM")
    private String name_DM;
    
    @Column(name = "image")
    private String image;
    
    @OneToMany(mappedBy = "danhMucSanPham")
    @JsonIgnore
    private List<SanPham> sanPhams;

	public Danh_Muc_SP() {
		super();
	}

	public Danh_Muc_SP(int idDanhMuc, String name_DM, String image, List<SanPham> sanPhams) {
		super();
		this.idDanhMuc = idDanhMuc;
		this.name_DM = name_DM;
		this.image = image;
		this.sanPhams = sanPhams;
	}

	public int getIdDanhMuc() {
		return idDanhMuc;
	}

	public void setIdDanhMuc(int idDanhMuc) {
		this.idDanhMuc = idDanhMuc;
	}

	public String getName_DM() {
		return name_DM;
	}

	public void setName_DM(String name_DM) {
		this.name_DM = name_DM;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public List<SanPham> getSanPhams() {
		return sanPhams;
	}

	public void setSanPhams(List<SanPham> sanPhams) {
		this.sanPhams = sanPhams;
	}

	@Override
	public String toString() {
		return "Danh_Muc_SP [idDanhMuc=" + idDanhMuc + ", name_DM=" + name_DM + ", image=" + image + ", sanPhams="
				+ sanPhams + "]";
	}

    
    
}
