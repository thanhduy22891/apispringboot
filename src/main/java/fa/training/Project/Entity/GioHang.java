package fa.training.Project.Entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Cart")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GioHang {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cart")
    private int id_cart;

    @ManyToOne
    @JoinColumn(name = "id_Kh")
    private Users user;

    @ManyToOne
    @JoinColumn(name = "id_SP")
    private SanPham sanPham;

    @Column(name = "quantity", nullable = false)
    private int quantity;

	public GioHang() {
		super();
	}

	public GioHang(int id_cart, Users user, SanPham sanPham, int quantity) {
		super();
		this.id_cart = id_cart;
		this.user = user;
		this.sanPham = sanPham;
		this.quantity = quantity;
	}

	public int getId_cart() {
		return id_cart;
	}

	public void setId_cart(int id_cart) {
		this.id_cart = id_cart;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public SanPham getSanPham() {
		return sanPham;
	}

	public void setSanPham(SanPham sanPham) {
		this.sanPham = sanPham;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "GioHang [id_cart=" + id_cart + ", user=" + user + ", sanPham=" + sanPham + ", quantity=" + quantity
				+ "]";
	}

   
	
	
	
	
}
