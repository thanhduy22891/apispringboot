package fa.training.Project.Entity;

import java.util.List;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Users {

	
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_Kh")
    private int id_Kh;

    @Column(name = "name_KH", nullable = false, unique = true)
    private String name_KH;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "email", nullable = false, unique = true)
    private String email;
    
    @OneToMany(mappedBy = "user")
    private List<GioHang> carts;

	public Users() {
		super();
	}

	public Users(int id_Kh, String name_KH, String password, String email, List<GioHang> carts) {
		super();
		this.id_Kh = id_Kh;
		this.name_KH = name_KH;
		this.password = password;
		this.email = email;
		this.carts = carts;
	}

	public int getId_Kh() {
		return id_Kh;
	}

	public void setId_Kh(int id_Kh) {
		this.id_Kh = id_Kh;
	}

	public String getName_KH() {
		return name_KH;
	}

	public void setName_KH(String name_KH) {
		this.name_KH = name_KH;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<GioHang> getCarts() {
		return carts;
	}

	public void setCarts(List<GioHang> carts) {
		this.carts = carts;
	}

	@Override
	public String toString() {
		return "Users [id_Kh=" + id_Kh + ", name_KH=" + name_KH + ", password=" + password + ", email=" + email
				+ ", carts=" + carts + "]";
	}

    
 
}


