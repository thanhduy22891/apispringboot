package fa.training.Project.Entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

	@Entity
	@Table(name = "thanh_toan")
	@AllArgsConstructor
	@NoArgsConstructor
	@Getter
	@Setter
	public class ThanhToan {

	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

	    @Column(name = "id_KH", nullable = false)
	    private int id_KH;

	    @Column(name = "name_KH", nullable = false)
	    private String name_KH;

	    @Column(name = "id_SP")
	    private int id_SP;

	    @Column(name = "name_SP", nullable = false)
	    private String name_SP;

	    @Column(name = "so_tien", nullable = false)
	    private Double soTien;

	    @ManyToOne
	    @JoinColumn(name = "TTTT", referencedColumnName = "tttt")
	    private TinhTrangThanhToan tinhTrang;

}
