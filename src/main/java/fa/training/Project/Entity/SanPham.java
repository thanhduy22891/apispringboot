package fa.training.Project.Entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "SanPham")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SanPham {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_SP")
	private int id_SP;

	@Column(name = "name_SP")
	private String name_SP;

	@Column(name = "image")
	private String image;

	@Column(name = "material")
	private String material;

	@Column(name = "price")
	private float price;

	@ManyToOne
	@JoinColumn(name = "idDanhMuc")
	private Danh_Muc_SP danhMucSanPham;

	public SanPham(int id_SP, String name_SP, String image, String material, float price, Danh_Muc_SP danhMucSanPham) {
		super();
		this.id_SP = id_SP;
		this.name_SP = name_SP;
		this.image = image;
		this.material = material;
		this.price = price;
		this.danhMucSanPham = danhMucSanPham;
	}

	public SanPham() {
		super();
	}

	public int getId_SP() {
		return id_SP;
	}

	public void setId_SP(int id_SP) {
		this.id_SP = id_SP;
	}

	public String getName_SP() {
		return name_SP;
	}

	public void setName_SP(String name_SP) {
		this.name_SP = name_SP;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public Danh_Muc_SP getDanhMucSanPham() {
		return danhMucSanPham;
	}

	public void setDanhMucSanPham(Danh_Muc_SP danhMucSanPham) {
		this.danhMucSanPham = danhMucSanPham;
	}

	@Override
	public String toString() {
		return "SanPham [id_SP=" + id_SP + ", name_SP=" + name_SP + ", image=" + image + ", material=" + material
				+ ", price=" + price + ", danhMucSanPham=" + danhMucSanPham + "]";
	}

}
