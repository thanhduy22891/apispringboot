package fa.training.Project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@EntityScan("fa.training.Project.Entity")
public class CunCoShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(CunCoShopApplication.class, args);
	}

}
